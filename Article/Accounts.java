package Article;

public class Accounts {
    String name;
    int balance;

    public Accounts(String name, int initialBalance) {
        this.name = name;
        this.balance = initialBalance;  
    }
    public Accounts(String name) {
        this(name, 0);
    }

    public void debitAmount(int amount){
        this.balance += amount;
    }
    public void creditAmount(int amount){
        this.balance -= amount;
    }

    public void checkTrans (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount){
        
    }

    public class Cash extends Accounts{

        public Cash(String name, int initialBalance) {
            super(name, initialBalance);
        }

        public void checkTrans (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount){
            if (this.equals(creditAccount) && (this.balance - creditAmount) < 0) {
                throw new IllegalArgumentException("Insufficient funds in Cash Accounts.");
            }
        }
    }

    public void entry (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount) {
        
        debitAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        creditAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        
        debitAccount.debitAmount(creditAmount);
        creditAccount.creditAmount(creditAmount);

    }
}

