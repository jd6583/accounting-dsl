package DSL;
import java.util.Date;

public class Transactions extends Books{


    // Inventory Purchase entry
    public void entry (Inventory debitAccount, int debitAmount, int quantity, Accounts creditAccount, int creditAmount, Date dateRecorded, Date dateTransact) {
    
        debitAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        creditAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        
        debitAccount.debitAmount(creditAmount, quantity);
        creditAccount.creditAmount(creditAmount);

        // Create a new transaction object
        Transaction transaction = new Transaction(debitAccount, debitAmount, creditAccount, creditAmount, dateRecorded, dateTransact);
        
        // Assuming you have a TransactionsLog instance accessible
        transactionsLog.addTransaction(transaction);
        }

    // Inventory Sale entry
    public void entry (Accounts debitAccount, int debitAmount, Inventory creditAccount, int creditAmount, int quantity, Date dateRecorded, Date dateTransact) {
    
        debitAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        creditAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        
        debitAccount.debitAmount(creditAmount);
        creditAccount.creditAmount(creditAmount, quantity);
        
        // Create a new InventoryTransaction object
        InventoryTransaction t = new InventoryTransaction(debitAccount, debitAmount, creditAccount, creditAmount, quantity, dateRecorded, dateTransact);

        // Assuming you have a TransactionsLog instance accessible
        transactionsLog.addTransaction(t);
    }
    
    // General entry
    public void entry (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount, Date dateRecorded, Date dateTransact) {
    
        debitAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        creditAccount.checkTrans(debitAccount, debitAmount, creditAccount, creditAmount);
        
        debitAccount.debitAmount(creditAmount);
        creditAccount.creditAmount(creditAmount);

        // Create a new InventoryTransaction object
        Transaction t = new Transaction(debitAccount, debitAmount, creditAccount, creditAmount, dateRecorded, dateTransact);

        // Add the transaction to the log
        transactionsLog.addTransaction(t);
    }
}
