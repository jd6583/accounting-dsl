package DSL;
import java.util.Date;

public class InventoryTransaction extends Transaction {
    private int quantity;

    public InventoryTransaction(Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount, int quantity, Date dateRecorded, Date dateTransact) {
        super(debitAccount, debitAmount, creditAccount, creditAmount,  dateRecorded,  dateTransact);
        this.quantity = quantity;
    }

}
