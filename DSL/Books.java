package DSL;
import java.util.ArrayList;
import java.util.List;

public class Books {

    // Nested TransactionsLog class inside Books
    public static class TransactionsLog {
        private List<Transaction> transactions;

        public TransactionsLog() {
            this.transactions = new ArrayList<>();
        }

        public void addTransaction(Transaction transaction) {
            transactions.add(transaction);
        }

        public List<Transaction> getTransactions() {
            return transactions;
        }
    }

    // Instance of TransactionsLog in Books
    protected TransactionsLog transactionsLog;

    public Books() {
        this.transactionsLog = new TransactionsLog();
    }

    // Access for TransactionsLog
    public TransactionsLog getTransactionsLog() {
        return transactionsLog;
    }

    public boolean audit(){
        return true;
    }
}
