package DSL;

public class Cash extends Accounts{

    public Cash(String name, int initialBalance) {
        super(name, initialBalance);
    }

    public void checkTrans (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount){
        if (this.equals(creditAccount) && (this.balance - creditAmount) < 0) {
            throw new IllegalArgumentException("Insufficient funds in Cash Accounts.");
        }
    }

}