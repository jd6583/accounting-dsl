package DSL;

public class DepreciableAssets extends Accounts {
    private double initialCost;
    private double salvageValue;
    private int usefulLifeYears;
    private double annualDepreciation;

    public DepreciableAssets(String name, double initialCost, double salvageValue, int usefulLifeYears) {
        super(name, (int) initialCost); // Passing initial cost as the initial balance for the account
        this.initialCost = initialCost;
        this.salvageValue = salvageValue;
        this.usefulLifeYears = usefulLifeYears;
        calculateAnnualDepreciation();
    }

    private void calculateAnnualDepreciation() {
        // Straight-line depreciation formula
        this.annualDepreciation = (initialCost - salvageValue) / usefulLifeYears;
    }

    public double getDepAmount() {
        return this.annualDepreciation;
    }

}
