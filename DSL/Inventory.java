package DSL;

public class Inventory extends Accounts{
    int quantity;
    
    public Inventory(String name, int initialBalance, int quantity) {
        super(name, initialBalance);
        this.quantity = quantity;
    }

    public void debitAmount(int amount, int quantity){
        this.balance += amount;
        this.quantity += quantity;
    }
    
    public void creditAmount(int amount, int quantity){
        this.balance -= amount;
        this.quantity -= quantity;
    }
    

}
