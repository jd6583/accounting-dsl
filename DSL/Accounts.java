package DSL;

public class Accounts extends Books{
    String name;
    int balance;
    

    public Accounts(String name, int initialBalance) {
        this.name = name;
        this.balance = initialBalance;  
    }
    public Accounts(String name) {
        this(name, 0);
    }

    public void debitAmount(int amount){
        this.balance += amount;
    }
    public void creditAmount(int amount){
        this.balance -= amount;
    }

    public void checkTrans (Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount){
        
    }

    
}

