package DSL;

import java.util.Date;

public class Transaction {
    private Accounts debitAccount;
    private Accounts creditAccount;
    private int debitAmount;
    private int creditAmount;
    private Date dateRecorded;
    private Date dateTransact;
    private String type; // For example, "purchase", "sale", etc.
    // Other details like transaction date, notes, etc., can be added here.

    // Constructor with all parameters
    public Transaction(Accounts debitAccount, int debitAmount, Accounts creditAccount, int creditAmount, Date dateRecorded, Date dateTransact) {
        this.debitAccount = debitAccount;
        this.debitAmount = debitAmount;
        this.creditAccount = creditAccount;
        this.creditAmount = creditAmount;
        this.dateRecorded = dateRecorded;
        this.dateTransact = dateTransact;
    }

}
