# Accounting DSL


## Domain-Specific Language for Accounting

## Description
Assessing the need for a domain-specific programming language for accounting. Designing user-friendly syntax and operational semantics for error-free accounting.

## Authors and acknowledgment
Lead Researcher - Janhavi Doshi
Thesis Advisor - Prof. Matthew Fluet

## Contributing
Please feel free to fork this repository, make changes, and submit pull requests. Any contributions, no matter how small, are greatly appreciated.

## License
This project is licensed under the MIT License. See the LICENSE file for more details.

## Contact
If you have any questions or feedback, please feel free to contact me at janhavi4696@gmail.com.